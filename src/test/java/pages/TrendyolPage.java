package pages;

import base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.ISuiteResult;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestNG;

import java.util.List;

public class TrendyolPage extends TestBase {

    private String productName = null;

    public void checkHomePagePopup(WebDriver driver) {

        if (checkWebelementIsDisabledWithXpath(driver, "homepagePopupXpath"))
            getWebElementWithXpath(driver, "homepagePopupCloseButtonXpath").click();
    }

    public void trendyolLogin(WebDriver driver)  throws InterruptedException{

        checkHomePagePopup(driver);
        waitForPageLoaded();
        waitUntilVisibleWithXpath(driver, "trendyolLoginButtonXpath");
        getWebElementWithXpath(driver, "trendyolLoginButtonXpath").click();
        Thread.sleep(1000);
        waitUntilPresenceOfElementByXpath(driver, "trendyolModalFacebookButtonXpath");
        getWebElementWithXpath(driver, "trendyolModalFacebookButtonXpath").click();

    }

    public void fillFacebookModal(WebDriver driver) {


        String winHandleBefore = driver.getWindowHandle();
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        waitForPageLoaded();
        waitUntilVisibleWithId(driver, "facebookLoginEmailId");
        getWebElementWithId(driver, "facebookLoginEmailId").sendKeys(config.getProperty("fbEmail"));
        waitUntilVisibleWithId(driver, "facebookLoginEmailId");
        getWebElementWithId(driver, "facebookLoginPasswordId").sendKeys(config.getProperty("fbPassword"));
        waitUntilElementClickableWithId(driver, "facebookLoginLoginButtonId");
        getWebElementWithId(driver, "facebookLoginLoginButtonId").click();
        if (checkWebelementIsDisabledWithXpath(driver, "facebookContinueButtonXpath")) {
            waitUntilElementClickableWithXpath(driver, "facebookContinueButtonXpath");
            getWebElementWithXpath(driver, "facebookContinueButtonXpath").click();
        }
        driver.switchTo().window(winHandleBefore);
    }

    public void searchProduct (WebDriver driver) throws Throwable {

        waitForPageLoaded();
        waitUntilVisibleWithId(driver, "searchBarId");
        waitUntilPresenceOfElementById(driver, "searchBarId");
        getWebElementWithId(driver, "searchBarId").sendKeys("Lacoste");
        waitUntilVisibleWithXpath(driver, "searchResultListXpath");
        List<WebElement> list = getListOfWebElementWithXpath(driver, "searchItemsXpath");
        for (int i = 0; i<list.size(); i++){

            if (list.get(i).getText().equalsIgnoreCase("lacoste")) {

                list.get(i).click();
                break;
            }
        }
        String productLabel = getWebElementWithXpath(driver, "searchProductLabelXpath").getText();
        if (productLabel.contains("Lacoste")) {

            System.out.println("This is right category");
            clickFavoriteProduct();
        } else {

            System.out.println("This is not right category");
            Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
        }

    }

    public void clickFavoriteProduct () throws Throwable {

        List <WebElement> products = getListOfWebElementWithXpath(driver, "searchResultProductList");
        String resultTitle = products.get(6).getText();
        products.get(6).click();
        productName = getWebElementWithXpath(driver, "productTitleXpath").getText();
        if (resultTitle.contains(productName)){

            System.out.println("This is right product");
            waitForPageLoaded();
            waitUntilVisibleWithXpath(driver, "productFavoriteButtonXpath");
            waitUntilElementClickableWithXpath(driver, "productFavoriteButtonXpath");
            Thread.sleep(1000);
            getWebElementWithXpath(driver, "productFavoriteButtonXpath").click();
        } else {

            System.out.println("This is not right product");
            Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
        }
    }

    public void myFavoriteProducts (WebDriver driver) {

        getWebElementWithId(driver, "myFavoritesId").click();
        waitUntilVisibleWithXpath(driver, "myFavoriteProductTitleXpath");
        if (productName.contains(getWebElementWithXpath(driver, "myFavoriteProductTitleXpath").getText()))
            System.out.println("This is right product");
            else {

            System.out.println("This is not right product");
            Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
        }
    }
}
