package base;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

public class TestBase {

    public static Properties config = null;
    public static Properties OR = null;
    public static WebDriver driver = null;
    private WebDriverWait jsWait;
    private JavascriptExecutor jsExec;

    public static void initializeConfigAndObjectRepository() throws IOException {

        config = new Properties();
        config.load(new FileInputStream(System.getProperty("user.dir") + "//src//etc//config.properties"));
        OR = new Properties();
        FileInputStream ip_OR = new FileInputStream(System.getProperty("user.dir") + "//src//etc//OR.properties");
        OR.load(ip_OR);
    }

    @BeforeSuite
    public synchronized void beforeSuite(ITestContext context) throws Throwable {

        initializeConfigAndObjectRepository();
    }

    @BeforeClass
    public void BeforeMethod() {
        System.setProperty("webdriver.gecko.driver",
                System.getProperty("user.dir") + "//geckodriver");
        System.setProperty("webdriver.chrome.driver",
                System.getProperty("user.dir") + "//chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to(config.getProperty("url"));
    }

    @AfterClass
    public void AfterMethod() {

        // driver.quit();
    }

    public void waitUntilElementClickableWithXpath(WebDriver driver, String xpath) {


        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofMillis(10)).ignoring(StaleElementReferenceException.class)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OR.getProperty(xpath))));
    }

    public void waitUntilElementClickableWithId(WebDriver driver, String id) {


        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofMillis(10)).ignoring(StaleElementReferenceException.class)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.elementToBeClickable(By.id(OR.getProperty(id))));
    }

    public void waitUntilVisibleWithXpath(WebDriver driver, String xpath) {


        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(45))
                .pollingEvery(Duration.ofMillis(10)).ignoring(StaleElementReferenceException.class)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OR.getProperty(xpath))));
    }

    public void waitUntilVisibleWithId(WebDriver driver, String id) {


        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofMillis(10)).ignoring(StaleElementReferenceException.class)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(OR.getProperty(id))));
    }


    public WebElement getWebElementWithXpath(WebDriver driver, String xpath) {

        WebElement ele = driver.findElement(By.xpath(OR.getProperty(xpath)));
        return ele;
    }

    public List<WebElement> getListOfWebElementWithXpath(WebDriver driver, String xpath) {

        List<WebElement> ele = driver.findElements(By.xpath(OR.getProperty(xpath)));
        return ele;
    }

    public WebElement getWebElementWithId(WebDriver driver, String xpath) {

        WebElement ele = driver.findElement(By.id(OR.getProperty(xpath)));
        return ele;
    }


    public Boolean checkWebelementIsDisabledWithXpath(WebDriver driver, String xpath) {

        try {
            return driver.findElement(By.xpath(OR.getProperty(xpath))).isDisplayed();
        } catch (Throwable t) {
            return false;
        }
    }


    public synchronized void waitUntilPresenceOfElementByXpath(WebDriver driver, String xpath) {

        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(50))
                .pollingEvery(Duration.ofMillis(10)).ignoring(StaleElementReferenceException.class)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(OR.getProperty(xpath))));

    }

    public synchronized void waitUntilPresenceOfElementById(WebDriver driver, String xpath) {

        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(50))
                .pollingEvery(Duration.ofMillis(10)).ignoring(StaleElementReferenceException.class)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(OR.getProperty(xpath))));

    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }
}
