package testCases;

import base.TestBase;
import org.testng.annotations.Test;
import pages.TrendyolPage;

public class Login extends TestBase {

    TrendyolPage trendyolPage = new TrendyolPage();

    @Test
    public void trendyolLogin () throws Throwable  {

        trendyolPage.trendyolLogin(driver);
        trendyolPage.fillFacebookModal(driver);
        trendyolPage.searchProduct(driver);
        trendyolPage.myFavoriteProducts(driver);
    }
}
